# translation of plasma_applet_trash.po to Estonian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2008, 2009, 2011, 2012, 2016.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_trash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-16 00:49+0000\n"
"PO-Revision-Date: 2016-07-27 17:50+0300\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/main.qml:103
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Ava"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Tühjenda"

#: contents/ui/main.qml:110
#, fuzzy, kde-format
#| msgid "Trash Settings..."
msgid "Trash Settings…"
msgstr "Prügikasti seadistused..."

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Prügikast\n"
"Tühi"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Prügikast\n"
"Üks element"
msgstr[1] ""
"Prügikast\n"
" %1 elementi"

#: contents/ui/main.qml:170
#, kde-format
msgid "Trash"
msgstr "Prügikast"

#: contents/ui/main.qml:171
#, kde-format
msgid "Empty"
msgstr "Tühi"

#: contents/ui/main.qml:171
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Üks element"
msgstr[1] "%1 elementi"

#~ msgid "Empty Trash"
#~ msgstr "Tühjenda prügikast"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "Do you really want to empty the trash? All items will be deleted."
#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Kas tõesti tühjendada prügikast? Kõik elemendid kustutatakse."

#~ msgid "Cancel"
#~ msgstr "Loobu"

#~ msgid "&Empty Trashcan"
#~ msgstr "Tühj&enda prügikast"

#~ msgid "&Menu"
#~ msgstr "&Menüü"

#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Prügikasti tühjendamine"

#~ msgid "Emptying Trashcan..."
#~ msgstr "Prügikasti tühjendamine..."
