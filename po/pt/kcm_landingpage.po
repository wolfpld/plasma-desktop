msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-12 00:48+0000\n"
"PO-Revision-Date: 2022-03-26 23:19+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr "Nome do esquema de cores"

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr "'Click' único para abrir os ficheiros"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr "Pacote de Aparência e Comportamento global"

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr "Pacote de Aparência e Comportamento global (alternativo)"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr "Velocidade da animação"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Theme:"
msgstr "Tema:"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Animation speed:"
msgstr "Velocidade da animação:"

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantânea"

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Change Wallpaper…"
msgstr "Mudar de Papel de Parede…"

#: package/contents/ui/main.qml:118
#, kde-format
msgid "More Appearance Settings…"
msgstr "Mais Configurações de Aparência…"

#: package/contents/ui/main.qml:133
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Ao carregar nos ficheiros ou pastas:"

#: package/contents/ui/main.qml:134
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Abre-os"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Seleccionar ao carregar no marcador de selecção do item"

#: package/contents/ui/main.qml:158
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Selecciona-os"

#: package/contents/ui/main.qml:172
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Abrir com duplo-click em alternativa"

#: package/contents/ui/main.qml:184
#, kde-format
msgid "More Behavior Settings…"
msgstr "Mais Configurações de Comportamento…"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Most Used Pages:"
msgstr "Páginas Mais Usadas:"

#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgid ""
#~ "You can help KDE improve Plasma by contributing information on how you "
#~ "use it, so we can focus on things that matter to you.<br/><br/"
#~ ">Contributing this information is optional and entirely anonymous. We "
#~ "never collect your personal data, files you use, websites you visit, or "
#~ "information that could identify you."
#~ msgstr ""
#~ "Poderá ajudar o KDE a melhorar o Plasma, contribuindo com informações "
#~ "sobre como o usa, para que nos possamos focar nas coisas importantes para "
#~ "si.<br/><br/>Contribuir com esta informação é opcional e é totalmente "
#~ "anónimo. Nunca recolhemos os seus dados pessoais, os ficheiros que usa, "
#~ "as páginas Web que visita ou outras informações que o possam identificar."

#~ msgid "No data will be sent."
#~ msgstr "Não serão enviados dados."

#~ msgid "The following information will be sent:"
#~ msgstr "Será enviada a seguinte informação:"

#~ msgid "Send User Feedback:"
#~ msgstr "Enviar as Reacções do Utilizador:"

#~ msgctxt "Adjective; as in, 'light theme'"
#~ msgid "Light"
#~ msgstr "Claro"

#~ msgctxt "Adjective; as in, 'dark theme'"
#~ msgid "Dark"
#~ msgstr "Escuro"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "José Nuno Pires"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zepires@gmail.com"

#~ msgid "Quick Settings"
#~ msgstr "Configurações Rápidas"

#~ msgid "Landing page with some basic settings."
#~ msgstr "Página inicial com algumas configurações rápidas."

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"
