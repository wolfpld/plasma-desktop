# translation of kaccess.po to Arabic
# translation of kaccess.po to
# Copyright (C) 2001,2002,2003, 2006, 2007, 2008 Free Software Foundation, Inc.
# Mohammed Gamal <f2c2001@yahoo.com>, 2001.
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002,2003.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# AbdulAziz AlSharif <a.a-a.s@hotmail.com>, 2007.
# Youssef Chahibi <chahibi@gmail.com>, 2007.
# Anas Husseini <linux.anas@gmail.com>, 2008.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-20 00:59+0000\n"
"PO-Revision-Date: 2021-06-18 23:11+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "محمد سعد, يوسف الشهيبي, أنس الحسيني, زايد السعيدي"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"metehyi@free.fr,chahibi@gmail.com,linux.anas@gmail.com,zayed.alsaidi@gmail."
"com"

#: kaccess.cpp:72
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr "أُقفل المفتاح Shift و هو الآن مفعّل لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:73
msgid "The Shift key is now active."
msgstr " المفتاح Shift مفعّل الآن."

#: kaccess.cpp:74
msgid "The Shift key is now inactive."
msgstr " المفتاح Shift غير مفعّل الآن."

#: kaccess.cpp:78
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr "تم إقفال المفتاح Control و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:79
msgid "The Control key is now active."
msgstr " المفتاح Control هو الآن نشط."

#: kaccess.cpp:80
msgid "The Control key is now inactive."
msgstr " المفتاح Control هو الآن خامل."

#: kaccess.cpp:84
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr "تمً إقفال المفتاح Alt و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:85
msgid "The Alt key is now active."
msgstr " المفتاح Alt هو الآن نشط."

#: kaccess.cpp:86
msgid "The Alt key is now inactive."
msgstr " المفتاح Alt هو الآن خامل."

#: kaccess.cpp:90
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr "تم إقفال المفتاح Win و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:91
msgid "The Win key is now active."
msgstr " المفتاح Win هو الآن نشط."

#: kaccess.cpp:92
msgid "The Win key is now inactive."
msgstr "المفتاح Win هو الآن خامل."

#: kaccess.cpp:96
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr "تم إقفال المفتاح Meta و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:97
msgid "The Meta key is now active."
msgstr "المفتاح Meta هو الآن نشط."

#: kaccess.cpp:98
msgid "The Meta key is now inactive."
msgstr "المفتاح Meta هو الآن خامل."

#: kaccess.cpp:102
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr "تمً إقفال المفتاح Super و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:103
msgid "The Super key is now active."
msgstr " المفتاح Super  هو الآن نشط."

#: kaccess.cpp:104
msgid "The Super key is now inactive."
msgstr " المفتاح Super  هو الآن خامل."

#: kaccess.cpp:108
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr "تمً إقفال المفتاح Hyper و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:109
msgid "The Hyper key is now active."
msgstr " المفتاح Hyper هو الآن نشط."

#: kaccess.cpp:110
msgid "The Hyper key is now inactive."
msgstr "المفتاح Hyper هو الآن خامل."

#: kaccess.cpp:114
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr "تم إقفال المفتاح Alt Gr و هو الآن نشط لكلّ ضغوطات المفاتيح التالية."

#: kaccess.cpp:115
msgid "The Alt Graph key is now active."
msgstr " المفتاح Alt Gr هو الآن نشط."

#: kaccess.cpp:116
msgid "The Alt Graph key is now inactive."
msgstr " المفتاح Alt Gr هو الآن خامل."

#: kaccess.cpp:117
msgid "The Num Lock key has been activated."
msgstr "تم تنشيط المفتاح Num Lock."

#: kaccess.cpp:117
msgid "The Num Lock key is now inactive."
msgstr " المفتاح Num Lock هو الآن نشط ."

#: kaccess.cpp:118
msgid "The Caps Lock key has been activated."
msgstr "تم تنشيط المفتاح Caps Lock."

#: kaccess.cpp:118
msgid "The Caps Lock key is now inactive."
msgstr "المفتاح Caps Lock هو الآن خامل."

#: kaccess.cpp:119
msgid "The Scroll Lock key has been activated."
msgstr "تم تنشيط المفتاح Scroll Lock."

#: kaccess.cpp:119
msgid "The Scroll Lock key is now inactive."
msgstr " المفتاح Scroll Lock هو الآن خامل."

#: kaccess.cpp:336
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr "شغل أو أغلق قارئ الشاشة"

#: kaccess.cpp:338
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr "الإتاحة"

#: kaccess.cpp:652
#, kde-format
msgid "AltGraph"
msgstr "AltGraph"

#: kaccess.cpp:654
#, kde-format
msgid "Hyper"
msgstr "Hyper"

#: kaccess.cpp:656
#, kde-format
msgid "Super"
msgstr "Super"

#: kaccess.cpp:658
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:675
#, kde-format
msgid "Warning"
msgstr "إنذار"

#: kaccess.cpp:703
#, kde-format
msgid "&When a gesture was used:"
msgstr "عند ا&ستخدام حركة:"

#: kaccess.cpp:709
#, kde-format
msgid "Change Settings Without Asking"
msgstr "غيّر الإعدادات بدون السؤال"

#: kaccess.cpp:710
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "أظهر حوار التأكيد هذا"

#: kaccess.cpp:711
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr "عطل كلّ ميزات وح&ركات AccessX"

#: kaccess.cpp:754 kaccess.cpp:756
#, kde-format
msgid "Slow keys"
msgstr "مفاتيح بطيئة"

#: kaccess.cpp:759 kaccess.cpp:761
#, kde-format
msgid "Bounce keys"
msgstr "مفاتيح قافزة"

#: kaccess.cpp:764 kaccess.cpp:766
#, kde-format
msgid "Sticky keys"
msgstr "ثبات المفاتيح"

#: kaccess.cpp:769 kaccess.cpp:771
#, kde-format
msgid "Mouse keys"
msgstr "مفاتيح الفأرة"

#: kaccess.cpp:778
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr "هل تريد فعلاً إبطال تنشيط \"%1\" ؟"

#: kaccess.cpp:781
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr "هل تريد فعلاً إبطال تنشيط \"%1\" و  \"%2\" ؟"

#: kaccess.cpp:785
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr "هل تريد فعلاً إبطال تنشيط \"%1\", \"%2\" و  \"%3\" ؟"

#: kaccess.cpp:788
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "هل تريد فعلاً إبطال تنشيط  \"%1\", \"%2\", \"%3\" و  \"%4\" ؟"

#: kaccess.cpp:799
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\" ؟"

#: kaccess.cpp:802
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\"  و إبطال تنشيط \"%2\" ؟"

#: kaccess.cpp:805
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\"  و إبطال تنشيط \"%2\" و  \"%3\" ؟"

#: kaccess.cpp:811
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\"  و إبطال تنشيط \"%2\", \"%3\" و \"%4\" ؟"

#: kaccess.cpp:822
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\" و  \"%2\" ؟"

#: kaccess.cpp:825
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\"  و \"%2\"  و إبطال تنشيط \"%3\" ؟"

#: kaccess.cpp:831
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\", و \"%2\" و إبطال تنشيط \"%3\" و \"%4\" ؟"

#: kaccess.cpp:842
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "هل تريد فعلاً تنشيط  \"%1\", \"%2\" و  \"%3\" ؟"

#: kaccess.cpp:845
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\", \"%2\" و \"%3\" و إبطال تنشيط \"%4\" ؟"

#: kaccess.cpp:854
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "هل تريد فعلاً تنشيط \"%1\", \"%2\", \"%3\"  و \"%4\" ؟"

#: kaccess.cpp:863
#, kde-format
msgid "An application has requested to change this setting."
msgstr "تطبيقُ ما طلب تغيير هذا الإعداد."

#: kaccess.cpp:867
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"قمت بالضغط على المفتاح Shift لأكثر من 8 ثوان أو أن تطبيقاً آخر  يطلب تغيير  "
"هذا الإعداد."

#: kaccess.cpp:869
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""
"قمت بالضغط على المفتاح Shift  مرات 5 متتالية أو أن تطبيقاً آخر  يطلب تغيير  "
"هذا الإعداد."

#: kaccess.cpp:873
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr "قمت بالضغط على المفتاح %1 أو أن تطبيقاً آخر  يطلب تغيير هذا الإعداد."

#: kaccess.cpp:878
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""
"أن تطبيقاً ما يطلب تغيير هذه الإعدادات، أو أنك قمت بتحريك عدة مفاتيح على لوحة "
"المفاتيح."

#: kaccess.cpp:880
#, kde-format
msgid "An application has requested to change these settings."
msgstr "أن تطبيقاً ما يطلب تغيير هذه الإعدادات."

#: kaccess.cpp:885
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""
"هذه الإعدادات AccessX هي محتاجة و مخصصة لبعض المستخدمين المعاقين بالحركات و "
"يمكن ضبطها في مركز  تحكم إعدادات كدي  . يمكنك أيضاً تشغيلها أو إيقافها بحركات "
"لوحة المفاتيح بشكل مقياسي.\n"
"\n"
"إذا لم تكن بحاجة إليها يمكنك  انتقاء \"عطل كلّ ميزات وحركات AccessX\"."

#: kaccess.cpp:906
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""
"مُكّنَتْ المفاتيح البطيئة. من الآن و صاعداً عليك ضغط كل مفتاح لمدة معينة من الوقت "
"حتى يجري مفعولها."

#: kaccess.cpp:908
#, kde-format
msgid "Slow keys has been disabled."
msgstr "عُطّلت المفاتيح البطيئة."

#: kaccess.cpp:912
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""
"مُكّنت المفاتيح المرتجّة. من الآن و صاعداً  نقر كل مفتاح  سيلغي مفعولها لمدة "
"معينة من الوقت بعد نقرها."

#: kaccess.cpp:914
#, kde-format
msgid "Bounce keys has been disabled."
msgstr "تم تعطيل المفاتيح القافزة."

#: kaccess.cpp:918
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""
"تم تمكين ثبات المفاتيح. من الآن وصاعداً, المفاتيح المغييره ستبقى مغلقة بعد "
"تحريرها."

#: kaccess.cpp:920
#, kde-format
msgid "Sticky keys has been disabled."
msgstr "تم تعطيل ثبات المفاتيح."

#: kaccess.cpp:924
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""
"تم تمكين مفاتيح الفأرة. من الآن وصاعداً, يمكنك استخدام وسادة الأرقام التابعة "
"للوحة المفاتيح للتحكم بالفأرة."

#: kaccess.cpp:926
#, kde-format
msgid "Mouse keys has been disabled."
msgstr "تم تعطيل مفاتيح الفأرة."

#: main.cpp:49
#, kde-format
msgid "Accessibility"
msgstr "الإتاحة"

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "(c) 2000, Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "المؤلف"

#~ msgid "KDE Accessibility Tool"
#~ msgstr "أداة إتاحة الوصول لـِ كيدي"

#~ msgid "kaccess"
#~ msgstr "kaccess"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Sticky Keys"
#~ msgstr "ثبات المفاتيح"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Use &sticky keys"
#~ msgstr "ثبات المفاتيح"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "&Lock sticky keys"
#~ msgstr "ثبات المفاتيح"

#, fuzzy
#~| msgid "Slow keys"
#~ msgid "&Use slow keys"
#~ msgstr "مفاتيح بطيئة"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Bounce Keys"
#~ msgstr "مفاتيح قافزة"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Use bou&nce keys"
#~ msgstr "مفاتيح قافزة"
