# Language  translations for KDE package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the KDE package.
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014, ٢٠١٥, ٢٠١٦.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: KDE 5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-26 00:59+0000\n"
"PO-Revision-Date: 2023-01-07 16:58+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr "مُستخدم حاليًّا"

#: contents/activitymanager/ActivityItem.qml:252
msgid ""
"Move to\n"
"this activity"
msgstr ""
"انقل إلى\n"
"هذا النشاط"

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"أظهر أيضا\n"
"في هذه النشاط"

#: contents/activitymanager/ActivityItem.qml:342
msgid "Configure"
msgstr "اضبط"

#: contents/activitymanager/ActivityItem.qml:359
msgid "Stop activity"
msgstr "أوقف النّشاط"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "الأنشطة الموقفة:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "أنشئ نشاطًا..."

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr "الأنشطة"

#: contents/activitymanager/StoppedActivityItem.qml:133
msgid "Configure activity"
msgstr "اضبط النّشاط"

#: contents/activitymanager/StoppedActivityItem.qml:148
msgid "Delete"
msgstr "احذف"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "للأسف! حصل خطأ أثناء تحميل %1."

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "انسخ إلى الحافظة"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "استعرض تفاصيل الخطأ…"

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr "افتح %1"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:248
msgid "About"
msgstr "عن"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "أرسِل بريد الإلكتروني إلى %1"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "افتح موقع %1"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "حقوق النسخ"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "الرّخصة:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "اعرض نص الترخيص"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "المؤلفين"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "الإشادات"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "المترجمون"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "بلغ عن علة…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "اختصارات لوحة المفاتيح"

#: contents/configuration/AppletConfiguration.qml:296
msgid "Apply Settings"
msgstr "طبّق الإعدادات"

#: contents/configuration/AppletConfiguration.qml:297
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr "تغيّرت إعدادات الوحدة الحاليّة. أتريد تطبيق الإعدادات أو رفضها؟"

#: contents/configuration/AppletConfiguration.qml:327
msgid "OK"
msgstr "حسنًا."

#: contents/configuration/AppletConfiguration.qml:335
msgid "Apply"
msgstr "طبّق"

#: contents/configuration/AppletConfiguration.qml:341
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "ألغ"

#: contents/configuration/ConfigCategoryDelegate.qml:28
msgid "Open configuration page"
msgstr "افتح صفح الضبط"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "الزّرّ الأيسر"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "الزّرّ الأيمن"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "الزّرّ الأوسط"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "زرّ العودة"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "زرّ التّقدّم"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "التّمرير الرّأسيّ"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "التّمرير الأفقيّ"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "حول"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "أضف إجراءً"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr "قُيد تغييرات التخطيط من قبل مسؤول النظام "

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "التّخطيط:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "نوع الخلفيّة:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "احصل على ملحقات جديدة..."

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr "يجب تطبيق تغييرات التخطيط قبل إجراء تغييرات أخرى"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "طبّق الآن"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "الاختصارات"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "سيؤدي هذا الاختصار إلى تنشيط التطبيق الصغير كما لو تم النقر عليه."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "الخلفيّة"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "إجراءات الفأرة"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "أدخل هنا"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:39
#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:194
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:192
msgid "Remove Panel"
msgstr "أزل اللوحة"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
msgid "Panel Alignment"
msgstr "محاذاة اللوحة"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Top"
msgstr "الأعلى"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Left"
msgstr "اليسار"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:73
msgid "Center"
msgstr "الوسط"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Bottom"
msgstr "الأسفل"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Right"
msgstr "اليمين"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:93
msgid "Visibility"
msgstr "الرّؤية"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Always Visible"
msgstr "أظهر دائمًا"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid "Auto Hide"
msgstr "أخف آليًّا"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:121
msgid "Windows Can Cover"
msgstr "يمكن للنّوافذ تغطيتها"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:128
msgid "Windows Go Below"
msgstr "النّوافذ تذهب تحتها"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:137
msgid "Opacity"
msgstr "العتمة"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:154
msgid "Adaptive"
msgstr "متكيف"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:161
msgid "Opaque"
msgstr "معتم"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:168
msgid "Translucent"
msgstr "شفاف"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid "Maximize Panel"
msgstr "كبّر اللوحة"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:184
msgid "Floating Panel"
msgstr "لوحة عائمة"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:200
msgid "Shortcut"
msgstr "الاختصار"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:213
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "اضغط على اختصار لوحة المفاتيح هذا لتنقل التركيز للوحة"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "اسحب لتغير الارتفاع الأقصى"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "اسحب لتغير العرض الأقصى"

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "انقر مزدوجًا للتصفير."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "اسحب لتغير الارتفاع الأدنى"

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "اسحب لتغير العرض الأدنى."

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"اسحب لتغير موضع على حافة هذه الشاشة.\n"
"انقر مزدوجاً للتصفير."

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "أضف ودجات..."

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "أضف مُباعد"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "خيارات أكثر..."

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "اسحب للتحريك"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "استخدم مفاتيح الأسهم لتحرك اللوحة"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "عرضة اللوحة:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "ارتفاع اللوحة:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "أغلق"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "إدارة أسطح المكتب واللوحات"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "يمكنك سحب اللوحات وأسطح المكتب لنقلها إلى شاشات مختلفة."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "بدل بسطح مكتب على الشاشة %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "انقل إلى الشّاشة %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:259
msgid "%1 (primary)"
msgstr "‏%1 (أولي)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "ودجات بديلة"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "بدّل"

#: contents/explorer/AppletDelegate.qml:175
msgid "Undo uninstall"
msgstr "تراجع عن إزالة التّثبيت"

#: contents/explorer/AppletDelegate.qml:176
msgid "Uninstall widget"
msgstr "أزل تثبيت الودجة"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "المؤلّف:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "البريد الإلكترونيّ:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "أزل التّثبيت"

#: contents/explorer/WidgetExplorer.qml:134
#: contents/explorer/WidgetExplorer.qml:245
msgid "All Widgets"
msgstr "كلّ الودجات"

#: contents/explorer/WidgetExplorer.qml:199
msgid "Widgets"
msgstr "الودجات"

#: contents/explorer/WidgetExplorer.qml:207
msgid "Get New Widgets…"
msgstr "اجلب ودجات جديدة..."

#: contents/explorer/WidgetExplorer.qml:256
msgid "Categories"
msgstr "الفئات"

#: contents/explorer/WidgetExplorer.qml:336
msgid "No widgets matched the search terms"
msgstr "لا توجد ودجات مطابقة لشروط البحث"

#: contents/explorer/WidgetExplorer.qml:336
msgid "No widgets available"
msgstr "لا تتوفر ودجات"

#~ msgid "%1 (disabled)"
#~ msgstr "‏%1 (معطّل)"

#~ msgid "Appearance"
#~ msgstr "المظهر"

#~ msgid "Search…"
#~ msgstr "ابحث..."

#~ msgid "Screen Edge"
#~ msgstr "حافّة الشّاشة"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr "انقر واسحب الزر إلى حافة الشاشة لتحريك اللوحة هناك."

#, fuzzy
#~| msgid "Apply Settings"
#~ msgid "Settings"
#~ msgstr "طبّق الإعدادات"

#~ msgid "Width"
#~ msgstr "العرض"

#~ msgid "Height"
#~ msgstr "الارتفاع"

#, fuzzy
#~| msgid "Layout cannot be changed whilst widgets are locked"
#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "لا يمكن تغير التّخطيط والودجات مقفلة"

#~ msgid "Lock Widgets"
#~ msgstr "اقفل الودجات"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "سينشّط هذا الاختصار البريمج: سيعطي تركيز لوحة المفاتيح إليه، وإن كان "
#~ "للبريمج مبثقة (كقائمة البدء)، ستُفتح المنبثقة هذه."

#~ msgid "Activity name:"
#~ msgstr "اسم النّشاط:"

#~ msgid "Create"
#~ msgstr "أنشئ"

#, fuzzy
#~| msgid "Are you sure you want to delete the activity?"
#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "أتريد حقًّا حذف النشاط؟"
