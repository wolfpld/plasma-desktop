# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-31 01:00+0000\n"
"PO-Revision-Date: 2022-09-26 19:02+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Избор на емотикони"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "Заменяне на съществуваща инстанция"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:52
#, kde-format
msgid "Search"
msgstr "Търсене"

#: app/ui/CategoryPage.qml:80
#, kde-format
msgid "Clear History"
msgstr "Изчистване на история"

#: app/ui/CategoryPage.qml:104
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Character"
msgstr ""

#: app/ui/CategoryPage.qml:107 app/ui/CategoryPage.qml:115
#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 е копиран в клипборда"

#: app/ui/CategoryPage.qml:112
#, kde-format
msgctxt "@item:inmenu"
msgid "Copy Description"
msgstr ""

#: app/ui/CategoryPage.qml:205
#, kde-format
msgid "No recent Emojis"
msgstr "Няма скорошни емотикони"

#: app/ui/emojier.qml:42
#, kde-format
msgid "Recent"
msgstr "Скоро отваряни"

#: app/ui/emojier.qml:63
#, kde-format
msgid "All"
msgstr "Всички"

#: app/ui/emojier.qml:71
#, kde-format
msgid "Categories"
msgstr "Категории"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr "Емотикони"

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr "Хора и човешко тяло"

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr "Компоненти"

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr "Природа и животни"

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr "Храна и напитки"

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr "Пътешествия"

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr "Дейности"

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr "Обекти"

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr "Символи"

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr "Флагове"
