# translation of kcmcomponentchooser.po to Slovenian
# Translation of kcmcomponentchooser.po to Slovenian
# SLOVENIAN TRANSLATION OF KDEBASE.
# $Id: kcm_componentchooser.po 1643780 2023-02-03 01:51:24Z scripty $
# $Source$
#
# Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.
# Gregor Rakar <gregor.rakar@kiss.uni-lj.si>, 2002.
# Gregor Rakar <gregor.rakar@kiss.si>, 2003, 2004, 2005.
# Jure Repinc <jlp@holodeck1.com>, 2006, 2007, 2008, 2009.
# Andrej Vernekar <andrej.vernekar@moj.net>, 2007.
# Andrej Mernik <andrejm@ubuntu.si>, 2013, 2014, 2016, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcmcomponentchooser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-03 01:01+0000\n"
"PO-Revision-Date: 2022-01-12 08:15+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Lokalize 21.04.3\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Gregor Rakar,Jure Repinc,Andrej Mernik,Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"gregor.rakar@kiss.si,jlp@holodeck1.com,andrejm@ubuntu.si,matjaz.jeran@amis."
"net"

#: applicationmodel.cpp:65
#, kde-format
msgid "Other…"
msgstr "Ostalo…"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "Privzeti spletni brskalnik"

#: components/componentchooserarchivemanager.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default archive manager"
msgstr "Izberi privzetega upravljalnika datotek"

#: components/componentchooserbrowser.cpp:15
#, kde-format
msgid "Select default browser"
msgstr "Izberi privzetega spletnega brskalnika"

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr "Izberi privzetega odjemalca e-pošte"

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr "Izberi privzetega upravljalnika datotek"

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr "Izberi privzeto mapo"

#: components/componentchooserimageviewer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default image viewer"
msgstr "Izberi privzetega upravljalnika datotek"

#: components/componentchoosermusicplayer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default map"
msgid "Select default music player"
msgstr "Izberi privzeto mapo"

#: components/componentchooserpdfviewer.cpp:11
#, fuzzy, kde-format
#| msgid "Select default browser"
msgid "Select default PDF viewer"
msgstr "Izberi privzetega spletnega brskalnika"

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr "Izberi privzeti program za klicanje telefonskih številk"

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr "Izberi privzeti terminalski program"

#: components/componentchoosertexteditor.cpp:15
#, fuzzy, kde-format
#| msgid "Select default terminal emulator"
msgid "Select default text editor"
msgstr "Izberi privzeti terminalski program"

#: components/componentchooservideoplayer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default video player"
msgstr "Izberi privzetega upravljalnika datotek"

#: kcm_componentchooser.cpp:68
#, kde-format
msgid "Component Chooser"
msgstr "Izbiralnik sestavnih delov"

#: package/contents/ui/ComponentOverlay.qml:23
#, kde-format
msgid "Details"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:29
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:43
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:51
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:60
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:66
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr ""

#: package/contents/ui/ComponentOverlay.qml:74
#, kde-format
msgid "Change file type association manually"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""

#: package/contents/ui/main.qml:45
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Web browser:"
msgstr "Spletni brskalnik:"

#: package/contents/ui/main.qml:71
#, kde-format
msgid "Email client:"
msgstr "Odjemalec elektronske pošte:"

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "Klicanje telefonskih številk:"

#: package/contents/ui/main.qml:115
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr ""

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Image viewer:"
msgstr ""

#: package/contents/ui/main.qml:144
#, kde-format
msgid "Music player:"
msgstr ""

#: package/contents/ui/main.qml:167
#, kde-format
msgid "Video player:"
msgstr ""

#: package/contents/ui/main.qml:189
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr ""

#: package/contents/ui/main.qml:194
#, kde-format
msgid "Text editor:"
msgstr ""

#: package/contents/ui/main.qml:216
#, kde-format
msgid "PDF viewer:"
msgstr ""

#: package/contents/ui/main.qml:238
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr ""

#: package/contents/ui/main.qml:243
#, kde-format
msgid "File manager:"
msgstr "Upravljalnik datotek:"

#: package/contents/ui/main.qml:265
#, kde-format
msgid "Terminal emulator:"
msgstr "Terminalski program:"

#: package/contents/ui/main.qml:278
#, fuzzy, kde-format
#| msgid "File manager:"
msgid "Archive manager:"
msgstr "Upravljalnik datotek:"

#: package/contents/ui/main.qml:300
#, fuzzy, kde-format
#| msgid "Map:"
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr "Mapa:"

#~ msgctxt "@title"
#~ msgid "Default Applications"
#~ msgstr "Privzete aplikacije"

#~ msgid "Joseph Wenninger"
#~ msgstr "Joseph Wenninger"

#~ msgid "Méven Car"
#~ msgstr "Méven Car"

#~ msgid "Tobias Fella"
#~ msgstr "Tobias Fella"

#~ msgid "Unknown"
#~ msgstr "Neznano"

#~ msgctxt "The label for the combobox: browser, terminal emulator...)"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "(c), 2002 Joseph Wenninger"
#~ msgstr "(c), 2002 Joseph Wenninger"

#~ msgid ""
#~ "Choose from the list below which component should be used by default for "
#~ "the %1 service."
#~ msgstr ""
#~ "Izberite iz spodnjega seznama sestavni del, ki naj se uporablja za "
#~ "storitev %1."

#~ msgid ""
#~ "<qt>You changed the default component of your choice, do want to save "
#~ "that change now ?</qt>"
#~ msgstr ""
#~ "<qt>Spremenili ste privzet sestavni del po svoji želji. Ali želite "
#~ "shraniti spremembo?</qt>"

#~ msgid "No description available"
#~ msgstr "Opis ni na voljo"

#~ msgid ""
#~ "Here you can change the component program. Components are programs that "
#~ "handle basic tasks, like the terminal emulator, the text editor and the "
#~ "email client. Different KDE applications sometimes need to invoke a "
#~ "console emulator, send a mail or display some text. To do so "
#~ "consistently, these applications always call the same components. You can "
#~ "choose here which programs these components are."
#~ msgstr ""
#~ "Tu lahko spremenite programske sestavne dele. To so programi, ki "
#~ "opravljajo osnovne naloge, kot so posnemovalnik terminala, urejevalnik "
#~ "besedil in odjemalec e-pošte. Različni programi za KDE morajo včasih "
#~ "poklicati posnemovalnik konzole, poslati pošto ali prikazati nekaj "
#~ "besedila. Da se to zgodi usklajeno, ti programi vedno prikličejo iste "
#~ "sestavne dele. Tu lahko izberete, kateri programi so ti sestavni deli."

#~ msgid "Default Component"
#~ msgstr "Privzet sestavni del"

#~ msgid ""
#~ "<qt>\n"
#~ "<p>This list shows the configurable component types. Click the component "
#~ "you want to configure.</p>\n"
#~ "<p>In this dialog you can change KDE default components. Components are "
#~ "programs that handle basic tasks, like the terminal emulator, the text "
#~ "editor and the email client. Different KDE applications sometimes need to "
#~ "invoke a console emulator, send a mail or display some text. To do so "
#~ "consistently, these applications always call the same components. Here "
#~ "you can select which programs these components are.</p>\n"
#~ "</qt>"
#~ msgstr ""
#~ "<qt>\n"
#~ "<p> Ta seznam prikazuje prilagodljive vrste sestavnih delov. Kliknite "
#~ "sestavni del, ki bi ga radi prilagodili.</p>\n"
#~ "<p>Tu lahko spremenite programske sestavne dele. To so programi, ki "
#~ "opravljajo osnovne naloge, kot so posnemovalnik terminala, urejevalnik "
#~ "besedil in odjemalec e-pošte. Različni programi za KDE morajo včasih "
#~ "poklicati posnemovalnik konzole, poslati pošto ali prikazati nekaj "
#~ "besedila. Da se to zgodi usklajeno, ti programi vedno prikličejo iste "
#~ "sestavne dele. Tu lahko izberete, kateri programi so ti sestavni deli.</"
#~ "p>\n"
#~ "</qt>"

#~ msgid "<qt>Open <b>http</b> and <b>https</b> URLs</qt>"
#~ msgstr "<qt>Odpri URL-je <b>http</b> in <b>https</b> </qt>"

#~ msgid "in an application based on the contents of the URL"
#~ msgstr "v programu na osnovi vsebine URL-ja"

#~ msgid "in the following application:"
#~ msgstr "v naslednjem programu:"

#~ msgid "with the following command:"
#~ msgstr "z naslednjim ukazom:"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Select preferred Web browser application:"
#~ msgstr "Izberite prednostni program za spletno brskanje:"

#~ msgid "Kmail is the standard Mail program for the Plasma desktop."
#~ msgstr "KMail je standarden poštni program za namizje Plasma."

#~ msgid "&Use KMail as preferred email client"
#~ msgstr "&Uporabi KMail kot prednostni odjemalec e-pošte"

#~ msgid "Select this option if you want to use any other mail program."
#~ msgstr ""
#~ "Izberite to možnost, če želite uporabiti katerikoli drug poštni program."

#~ msgid ""
#~ "Optional placeholders: <ul> <li>%t: Recipient's address</li> <li>%s: "
#~ "Subject</li> <li>%c: Carbon Copy (CC)</li> <li>%b: Blind Carbon Copy "
#~ "(BCC)</li> <li>%B: Template body text</li> <li>%A: Attachment </li> <li>"
#~ "%u: Full mailto: URL </li></ul>"
#~ msgstr ""
#~ "Izbirni vsebniki: <ul> <li>%t: Naslov prejemnika</li> <li>%s: Zadeva</li> "
#~ "<li>%c: Kopijo prejme (Kp)</li> <li>%b: Skrito kopijo prejme (Skp)</li> "
#~ "<li>%B: Predloga besedila telesa</li> <li>%A: Priloga </li> <li>%u: Poln "
#~ "naslov pošiljanja (»mailto«): URL </li></ul>"

#~ msgid ""
#~ "Press this button to select your favorite email client. Please note that "
#~ "the file you select has to have the executable attribute set in order to "
#~ "be accepted.<br/> You can also use several placeholders which will be "
#~ "replaced with the actual values when the email client is called:<ul> <li>"
#~ "%t: Recipient's address</li> <li>%s: Subject</li> <li>%c: Carbon Copy "
#~ "(CC)</li> <li>%b: Blind Carbon Copy (BCC)</li> <li>%B: Template body "
#~ "text</li> <li>%A: Attachment </li> </ul>"
#~ msgstr ""
#~ "Kliknite ta gumb za izbiro vašega najljubšega e-poštnega odjemalca. "
#~ "Zapomnite si, da mora imeti izbrana datoteka nastavljen atribut za "
#~ "izvajanje, da bo sprejeta.<br /> Lahko tudi uporabite več vsebnikov, ki "
#~ "bodo zamenjani takrat, ko bo e-poštni odjemalec poklican.<ul> <li>%t: "
#~ "Naslov prejemnika</li> <li>%s: Zadeva</li> <li>%c: Kopijo prejme (Kp)</"
#~ "li> <li>%b: Skrito kopijo prejme (Skp)</li> <li>%B: Besedilo predloge "
#~ "telesa</li> <li>%A: Priloga </li> </ul>"

#~ msgid "Click here to browse for the mail program file."
#~ msgstr "Kliknite tukaj, da bi poiskali programsko datoteko za pošto."

#~ msgid ""
#~ "Activate this option if you want the selected email client to be executed "
#~ "in a terminal (e.g. <em>Konsole</em>)."
#~ msgstr ""
#~ "Omogočite to možnost, če želite izbran odjemalec e-pošte zagnati v "
#~ "terminalu (npr. <em>Konsole</em>)."

#~ msgid "&Run in terminal"
#~ msgstr "&Zaženi v terminalu"

#~ msgid "Browse directories using the following file manager:"
#~ msgstr "Po mapah brskaj z naslednjim upravljalnikom datotek:"

#~ msgid "Other: click Add... in the dialog shown here:"
#~ msgstr "Drug: kliknite »Dodaj ...« v tukaj prikazanem pogovornem oknu:"

#~ msgid "&Use Konsole as terminal application"
#~ msgstr "&Uporabi Konsole kot terminalski program"

#~ msgid "Use a different &terminal program:"
#~ msgstr "Uporabi drug &terminalski program:"

#~ msgid ""
#~ "Press this button to select your favorite terminal client. Please note "
#~ "that the file you select has to have the executable attribute set in "
#~ "order to be accepted.<br/> Also note that some programs that utilize "
#~ "Terminal Emulator will not work if you add command line arguments "
#~ "(Example: konsole -ls)."
#~ msgstr ""
#~ "Pritisnite ta gumb, da izberete vašega najljubšega terminalskega "
#~ "odjemalca. Zapomnite si, da mora imeti izbrana datoteka nastavljen "
#~ "atribut izvedljivosti, da je sprejeta.<br />Vedite tudi, da nekateri "
#~ "programi, ki uporabljajo posnemovalnik terminala, ne bodo delovali, če "
#~ "dodate argumente ukazne vrstice (primer: konsole -ls)."

#~ msgid "Click here to browse for terminal program."
#~ msgstr "Kliknite tukaj, da bi poiskali terminalski program."

#~ msgid "1 second remaining:"
#~ msgid_plural "%1 seconds remaining:"
#~ msgstr[0] "Še %1 sekund:"
#~ msgstr[1] "Še %1 sekunda:"
#~ msgstr[2] "Še %1 sekundi:"
#~ msgstr[3] "Še %1 sekunde:"

#~| msgid ""
#~| "The new window manager will be used when KDE is started the next time."
#~ msgid ""
#~ "The new window manager will be used when Plasma is started the next time."
#~ msgstr ""
#~ "Novi upravljalnik oken bo uporabljen po naslednjem ponovnem zagonu Plasme."

#~ msgid "Window Manager Change"
#~ msgstr "Sprememba upravljalnika oken"

#~ msgid ""
#~ "A new window manager is running.\n"
#~ "It is still recommended to restart this KDE session to make sure all "
#~ "running applications adjust for this change."
#~ msgstr ""
#~ "Zagnan je nov upravljalnik oken.\n"
#~ "Da se vsi zagnani programi zagotovo prilagodijo spremembi, je priporočeno "
#~ "znova zagnati to sejo KDE."

#~ msgid "Window Manager Replaced"
#~ msgstr "Upravljalnik oken zamenjan"

#~ msgid ""
#~ "Your running window manager will be now replaced with the configured one."
#~ msgstr "Trenutno zagnan upravljalnik oken bo sedaj zamenjan z nastavljenim."

#~ msgid "Config Window Manager Change"
#~ msgstr "Sprememba nastavljenega upravljalnika oken"

#~ msgid "&Accept Change"
#~ msgstr "&Sprejmi spremembo"

#~ msgid "&Revert to Previous"
#~ msgstr "&Povrni prejšnjega"

#~ msgid ""
#~ "The configured window manager is being launched.\n"
#~ "Please check it has started properly and confirm the change.\n"
#~ "The launch will be automatically reverted in 20 seconds."
#~ msgstr ""
#~ "Zagnan je bil novo nastavljeni upravljalnik oken.\n"
#~ "Preverite, ali deluje pravilno, in potrdite spremembo.\n"
#~ "Sprememba bo čez 20 sekund samodejno razveljavljena."

#~ msgid ""
#~ "The running window manager has been reverted to the default KDE window "
#~ "manager KWin."
#~ msgstr ""
#~ "Uporabljeni upravljalnik oken je bil povrnjen na privzeti KDE-jev "
#~ "upravljalnik oken KWin."

#~ msgid ""
#~ "The new window manager has failed to start.\n"
#~ "The running window manager has been reverted to the default KDE window "
#~ "manager KWin."
#~ msgstr ""
#~ "Novega upravljalnika oken ni bilo mogoče zagnati.\n"
#~ "Uporabljeni upravljalnik oken je bil povrnjen na privzeti KDE-jev "
#~ "upravljalnik oken KWin."

#~ msgid "Running the configuration tool failed"
#~ msgstr "Ni bilo mogoče zagnati nastavitvenega orodja"

#~ msgid "&Use the default KDE window manager (KWin)"
#~ msgstr "&Uporabi privzeti KDE-jev upravljalnik oken (KWin)"

#~ msgid "Use a different &window manager:"
#~ msgstr "Uporabi drug up&ravljalnik oken:"

#~ msgid "Configure"
#~ msgstr "Nastavi"

#~ msgid ""
#~ "Note: Most window managers have their own configuration and do not follow "
#~ "KDE settings."
#~ msgstr ""
#~ "Opomba: večina upravljalnikov oken ima svoje nastavitve in ne sledijo "
#~ "nastavitvam v KDE."

#~ msgid "kcmcomponentchooser"
#~ msgstr "kcmcomponentchooser"
