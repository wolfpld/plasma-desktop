# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.kimpanel\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2017-05-22 05:33-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "מראה"

#: package/contents/ui/ActionMenu.qml:93
#, kde-format
msgid "(Empty)"
msgstr "(ריק)"

#: package/contents/ui/ConfigAppearance.qml:29
#, fuzzy, kde-format
#| msgid "Exit Input Method"
msgid "Input method list:"
msgstr "יציאה משיטת הקלט"

#: package/contents/ui/ConfigAppearance.qml:30
#, fuzzy, kde-format
#| msgid "Vertical List"
msgid "Vertical"
msgstr "רשימה אנכית"

#: package/contents/ui/ConfigAppearance.qml:36
#, kde-format
msgid "Horizontal"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:43
#, kde-format
msgid "Font:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:49
#, kde-format
msgid "Use custom:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:55
#, kde-format
msgctxt "The selected font family and font size"
msgid " "
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:67
#, fuzzy, kde-format
#| msgctxt "@title:window"
#| msgid "Select Font"
msgid "Select Font…"
msgstr "בחירת גופן"

#: package/contents/ui/ConfigAppearance.qml:73
#, kde-format
msgctxt "The arrangement of icons in the Panel"
msgid "Panel icon size:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:74
#, kde-format
msgid "Small"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:81
#, kde-format
msgid "Scale with Panel height"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:82
#, kde-format
msgid "Scale with Panel width"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:90
#, kde-format
msgctxt "@title:window"
msgid "Select Font"
msgstr "בחירת גופן"

#: package/contents/ui/ContextMenu.qml:83
#, kde-format
msgid "Show"
msgstr "הצג"

#: package/contents/ui/ContextMenu.qml:98
#, kde-format
msgid "Hide %1"
msgstr "הסתר את %1"

#: package/contents/ui/ContextMenu.qml:107
#, kde-format
msgid "Configure Input Method"
msgstr "הגדרת שיטת קלט"

#: package/contents/ui/ContextMenu.qml:115
#, kde-format
msgid "Reload Config"
msgstr "טען מחדש הגדרות"

#: package/contents/ui/ContextMenu.qml:123
#, kde-format
msgid "Exit Input Method"
msgstr "יציאה משיטת הקלט"

#: package/contents/ui/main.qml:237
#, fuzzy, kde-format
#| msgid "Exit Input Method"
msgid "Input Method Panel"
msgstr "יציאה משיטת הקלט"

#, fuzzy
#~| msgid "Select Font"
#~ msgid "Select Font..."
#~ msgstr "בחר גופן"

#~ msgid "Use Default Font"
#~ msgstr "השתמש בגופן ברירת המחדל"

#~ msgid "Custom Font:"
#~ msgstr "גופן מותאם אישית:"
