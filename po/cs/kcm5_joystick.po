# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2014.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: joystick\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:48+0000\n"
"PO-Revision-Date: 2021-06-10 17:46+0200\n"
"Last-Translator: Vít Pelčák <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 3.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Klára Cihlářová"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "koty@seznam.cz"

#: caldialog.cpp:26 joywidget.cpp:335
#, kde-format
msgid "Calibration"
msgstr "Kalibrace"

#: caldialog.cpp:40
#, kde-format
msgid "Next"
msgstr "Následující"

#: caldialog.cpp:50
#, kde-format
msgid "Please wait a moment to calculate the precision"
msgstr "Prosím, vyčkejte než se vypočítá přesnost"

#: caldialog.cpp:79
#, kde-format
msgid "(usually X)"
msgstr "(obvykle X)"

#: caldialog.cpp:81
#, kde-format
msgid "(usually Y)"
msgstr "(obvykle Y)"

#: caldialog.cpp:87
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>minimum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Kalibrace spočívá v kontrole rozsahu hodnot zařízení.<br /><br /"
">Přesuňte <b>osu %1 %2</b> zařízení na <b>minimum</b>.<br /><br />Stisknutím "
"libovolného tlačítka na zařízení nebo tlačítka 'Následující' přejdete na "
"následující krok.</qt>"

#: caldialog.cpp:110
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>center</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Kalibrace spočívá v kontrole rozsahu hodnot zařízení.<br /><br /"
">Přesuňte <b>osu %1 %2</b> zařízení na <b>střed</b>.<br /><br />Stisknutím "
"libovolného tlačítka na zařízení nebo tlačítka 'Následující' přejdete na "
"následující krok.</qt>"

#: caldialog.cpp:133
#, kde-format
msgid ""
"<qt>Calibration is about to check the value range your device delivers.<br /"
"><br />Please move <b>axis %1 %2</b> on your device to the <b>maximum</b> "
"position.<br /><br />Press any button on the device or click on the 'Next' "
"button to continue with the next step.</qt>"
msgstr ""
"<qt>Kalibrace spočívá v kontrole rozsahu hodnot zařízení.<br /><br /"
">Přesuňte <b>osu %1 %2</b> zařízení na <b>maximum</b>.<br /><br />Stisknutím "
"libovolného tlačítka na zařízení nebo tlačítka 'Následující' přejdete na "
"následující krok.</qt>"

#: caldialog.cpp:160 joywidget.cpp:325 joywidget.cpp:361
#, kde-format
msgid "Communication Error"
msgstr "Chyba v komunikaci"

#: caldialog.cpp:164
#, kde-format
msgid "You have successfully calibrated your device"
msgstr "Zařízení bylo úspěšně kalibrováno"

#: caldialog.cpp:164 joywidget.cpp:363
#, kde-format
msgid "Calibration Success"
msgstr "Úspěšná kalibrace"

#: caldialog.cpp:184
#, kde-format
msgid "Value Axis %1: %2"
msgstr "Hodnota na ose %1: %2"

#: joydevice.cpp:41
#, kde-format
msgid "The given device %1 could not be opened: %2"
msgstr "Nelze otevřít dané zařízení %1: %2"

#: joydevice.cpp:45
#, kde-format
msgid "The given device %1 is not a joystick."
msgstr "Dané zařízení %1 není joystickem."

#: joydevice.cpp:49
#, kde-format
msgid "Could not get kernel driver version for joystick device %1: %2"
msgstr "Nelze získat verzi ovladače z jádra pro zařízení %1: %2"

#: joydevice.cpp:60
#, kde-format
msgid ""
"The current running kernel driver version (%1.%2.%3) is not the one this "
"module was compiled for (%4.%5.%6)."
msgstr ""
"Aktuální běžící verze ovladače z jádra (%1.%2.%3) není určena pro tento "
"modul (%4.%5.%6)."

#: joydevice.cpp:71
#, kde-format
msgid "Could not get number of buttons for joystick device %1: %2"
msgstr "Nelze zjistit počet tlačítek zařízení %1: %2"

#: joydevice.cpp:75
#, kde-format
msgid "Could not get number of axes for joystick device %1: %2"
msgstr "Nelze zjistit počet os zařízení %1: %2"

#: joydevice.cpp:79
#, kde-format
msgid "Could not get calibration values for joystick device %1: %2"
msgstr "Nelze zjistit kalibrační hodnoty zařízení %1: %2"

#: joydevice.cpp:83
#, kde-format
msgid "Could not restore calibration values for joystick device %1: %2"
msgstr "Nelze obnovit kalibrační hodnoty zařízení %1: %2"

#: joydevice.cpp:87
#, kde-format
msgid "Could not initialize calibration values for joystick device %1: %2"
msgstr "Nelze inicializovat kalibrační hodnoty zařízení %1: %2"

#: joydevice.cpp:91
#, kde-format
msgid "Could not apply calibration values for joystick device %1: %2"
msgstr "Nelze aplikovat kalibrační hodnoty zařízení %1: %2"

#: joydevice.cpp:95
#, kde-format
msgid "internal error - code %1 unknown"
msgstr "interní chyba - neznámý kód %1"

#: joystick.cpp:29
#, kde-format
msgid "KDE Joystick Control Module"
msgstr "KDE modul nastavení joysticku"

#: joystick.cpp:31
#, kde-format
msgid "KDE System Settings Module to test Joysticks"
msgstr "KDE modul ovládacího centra pro test joysticků"

#: joystick.cpp:33
#, kde-format
msgid "(c) 2004, Martin Koller"
msgstr "(c) 2004, Martin Koller"

#: joystick.cpp:38
#, kde-format
msgid ""
"<h1>Joystick</h1>This module helps to check if your joystick is working "
"correctly.<br />If it delivers wrong values for the axes, you can try to "
"solve this with the calibration.<br />This module tries to find all "
"available joystick devices by checking /dev/js[0-4] and /dev/input/"
"js[0-4]<br />If you have another device file, enter it in the combobox.<br /"
">The Buttons list shows the state of the buttons on your joystick, the Axes "
"list shows the current value for all axes.<br />NOTE: the current Linux "
"device driver (Kernel 2.4, 2.6) can only autodetect<ul><li>2-axis, 4-button "
"joystick</li><li>3-axis, 4-button joystick</li><li>4-axis, 4-button "
"joystick</li><li>Saitek Cyborg 'digital' joysticks</li></ul>(For details you "
"can check your Linux source/Documentation/input/joystick.txt)"
msgstr ""
"<h1>Joystick</h1>Tento modul vám pomůže zjistit, zda váš joystick pracuje "
"správně.<br />Pokud předává chybné hodnoty os, můžete problém vyřešit "
"kalibrací.<br />Modul se také pokusí vyhledat všechna dostupná zařízení "
"kontrolou /dev/js[0-4] a /dev/input/js[0-4]<br /> Pokud je vaše zařízení "
"připojené jinde, zadejte jméno zařízení.<br /> Seznam tlačítek obsahuje stav "
"tlačítek vašeho joysticku, seznam os pak aktuální nastavené hodnoty pro osy."
"<br />POZNÁMKA: aktuální linuxový ovladač (jádra 2.4, 2.6) umí automaticky "
"detekovat pouze <ul><li>2-osý, 4-tlačítkový joystick</li><li>3-osý, 4-"
"tlačítkový joystick</li><li>4-osý, 4-tlačítkový joystick</li><li>Saitek "
"Cyborg 'digital' joysticky</li></ul>(Více informací najdete v adresáři se "
"zdrojovými kódy jádra v podadresáři /Documentation/input/joystick.txt)"

#: joywidget.cpp:67
#, kde-format
msgid "Device:"
msgstr "Zařízení:"

#: joywidget.cpp:84
#, kde-format
msgctxt "Cue for deflection of the stick"
msgid "Position:"
msgstr "Pozice:"

#: joywidget.cpp:87
#, kde-format
msgid "Show trace"
msgstr "Vypsat trace"

#: joywidget.cpp:96 joywidget.cpp:300
#, kde-format
msgid "PRESSED"
msgstr "STISKNUTO"

#: joywidget.cpp:98
#, kde-format
msgid "Buttons:"
msgstr "Tlačítka:"

#: joywidget.cpp:102
#, kde-format
msgid "State"
msgstr "Stav"

#: joywidget.cpp:110
#, kde-format
msgid "Axes:"
msgstr "Osy:"

#: joywidget.cpp:114
#, kde-format
msgid "Value"
msgstr "Hodnota"

#: joywidget.cpp:127
#, kde-format
msgid "Calibrate"
msgstr "Kalibrovat"

#: joywidget.cpp:190
#, kde-format
msgid ""
"No joystick device automatically found on this computer.<br />Checks were "
"done in /dev/js[0-4] and /dev/input/js[0-4]<br />If you know that there is "
"one attached, please enter the correct device file."
msgstr ""
"Automaticky nebyl nalezen žádný připojený joystick.<br />Překontrolujte /dev/"
"js[0-4] a /dev/input/js[0-4]<br />Pokud jste si jistí, že je joystick "
"správně připojen, zadejte správný soubor zařízení."

#: joywidget.cpp:226
#, kde-format
msgid ""
"The given device name is invalid (does not contain /dev).\n"
"Please select a device from the list or\n"
"enter a device file, like /dev/js0."
msgstr ""
"Zadaný název zařízení je neplatný (neobsahuje část /dev).\n"
"Zvolte prosím zařízení ze seznamu nebo zadejte\n"
"název souboru zařízení např. /dev/js0."

#: joywidget.cpp:229
#, kde-format
msgid "Unknown Device"
msgstr "Neznámé zařízení"

#: joywidget.cpp:247
#, kde-format
msgid "Device Error"
msgstr "Chyba zařízení"

#: joywidget.cpp:265
#, kde-format
msgid "1(x)"
msgstr "1(x)"

#: joywidget.cpp:266
#, kde-format
msgid "2(y)"
msgstr "2(y)"

#: joywidget.cpp:331
#, kde-format
msgid ""
"<qt>Calibration is about to check the precision.<br /><br /><b>Please move "
"all axes to their center position and then do not touch the joystick anymore."
"</b><br /><br />Click OK to start the calibration.</qt>"
msgstr ""
"<qt>Kalibrace spočívá ve zjištění přesnosti.<br /><br /><b> Přesuňte všechny "
"osy do jejich centrální polohy a dále se již joysticku nedotýkejte. </b><br /"
"><br />Kalibraci spustíte stisknutím tlačítka OK.</qt>"

#: joywidget.cpp:363
#, kde-format
msgid "Restored all calibration values for joystick device %1."
msgstr "Všechny kalibrační hodnoty zařízení %1 byly obnoveny."
